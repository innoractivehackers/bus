<?php
return [
    'menu' => "Help command:".PHP_EOL
             .'/help = Show this help'.PHP_EOL
                    .'/channel = Show list of channel to subscribe'.PHP_EOL
                    .'/sub {channel name} = Subscribe to {channel name}'.PHP_EOL
                    .'/listsub = Show list of channel you have subscribed'.PHP_EOL
                    .'/unsub {channel name} = Un-Subscribe to {channel name}',
    'channel' => "Channel",
    'create_success' => "created successfully.",
    'fail_create' => "failed to create.",
    'exist' => "existed or conflict with parent channel. No action.",
    'unsubscribe' => "unsubscribe successfully.",
    'not_exist' => "does not exist.",
    'channel_list' => "Channel list",
    ];