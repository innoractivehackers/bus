<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$app->get('/', function () use ($app) {
    return file_get_contents("intro.html" );
});

// Testing server response and telegram test msg.
$app->get('test','BusController@testMsg');

// Telegram webhook
$app->post('webhook','BusController@webhook');
$app->get('webhook','BusController@testWebhook');

// Incoming email webhook
$app->post('mailhook','BusController@mailhook');
$app->get('mailhook','BusController@testMailhook');