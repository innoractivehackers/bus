# PHP Email to Popular Instant Messaging App (Telegram)
[![Minimum PHP Version](http://img.shields.io/badge/php-%3E%3D5.6-8892BF.svg)](https://php.net/)
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>

## Table of Contents
- [Introduction](#introduction)
- [Instructions](#instructions)
- [Web Hook](#Webhooks)
- [Usage](#usage)
- [Security](#security)
- [To Do](#todo)
- [Known bugs](#bugs)
- [Donate](#donate)
- [License](#license)



## Introduction

This is a PHP Email to Popular Instant Messaging App Bot, allowing integrators of
all sorts to bring automated interactions to the mobile platform. This
Bot provide a platform where one can subscribe to channel based on email criteria. 

## Instructions

<b>Environment Configuration:</b><br>

Rename `.env.example` to `.env`.
`BOT_TOKEN={bot token}`<br><br>
For debugging purpose, set this to true and it will send to <i>DEFAULT_USER_ID</i><br>
`TELEGRAM_SEND_TOSELF=true` <br>
`DEFAULT_USER_ID={telegram user ID}`

To join bot, search for `@innobus_bot` or http://t.me/innobus_bot and start conversation.

## Web hooks

To set the Telegram to point to your server webhook, go to internet browser and perform following action

**Telegram:**

`https://api.telegram.org/bot{bot_key}/setWebhook?url={webhook url}`

**Example**

`https://api.telegram.org/bot533444444:hjkhdfgfjhjhfjhjhjhjhjhjhjhhjjhw/setWebhook?url=https://bus.innoractive.com/webhook`

If success set, it will response: 
`{"ok":true,"result":true,"description":"Webhook was set"}
`

**Mail webhook (Tested on Mailgun):**

Using Routes, specify the `Catch-all`/`Match recipient`/`Match Header`/`Custom` to point to 
`{server url}/mailhook`

**Example**

`https://bus.innoractive.com/mailhook`
## Usage

**List of command:**

`/help` = Show list of command

`/channel` = Show list of channel to subscribe

`/sub {channel name}` = Subscribe to {channel name}. If no channel name specified, it will list down you have subscribed.

`/listsub` = Show list of channel you have subscribed

`/unsub {channel name}` = Un-Subscribe to {channel name}


## Security

If you discover any security related issues, please email davidliaw@innoractive.com instead of using the issue tracker.

## To Do

- Authentication
- LINE app support.

## Known Bugs

-
## Donate

All work on this bot consists of many hours of coding during our free time, to provide you with a Email to instant messaging library that is easy to use and extend.


## License

Please see the [LICENSE](LICENSE.md) included in this repository for a full copy of the MIT license,
which this project is licensed under.

