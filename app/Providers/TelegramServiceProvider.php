<?php

namespace App\Providers;
use Illuminate\Support\ServiceProvider;


class TelegramServiceProvider extends ServiceProvider
{
    /**
     * Register Telegram services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\TelegramServices','App\Services\TelegramServices' );
    }

}
