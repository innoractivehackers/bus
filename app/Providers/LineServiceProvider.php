<?php

namespace App\Providers;
use Illuminate\Support\ServiceProvider;


class LineServiceProvider extends ServiceProvider
{
    /**
     * Register Telegram services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\LineServices','App\Services\LineServices' );
    }

}
