<?php

namespace App\Providers;
use Illuminate\Support\ServiceProvider;


class EmailParsingServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\EmailParsingServices','App\Services\EmailParsingServices' );
    }

}
