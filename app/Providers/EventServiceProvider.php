<?php

namespace App\Providers;

use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SendMsgEvent' => [
            'App\Listeners\SendTelegMsgListener',
            'App\Listeners\AuditMsgListener',
        ],
        'App\Events\RecvdMsgEvent' => [
            'App\Listeners\AuditInMsgListener',
        ],
    ];
}
