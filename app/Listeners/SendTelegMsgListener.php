<?php

namespace App\Listeners;

use App\Events\SendMsgEvent;
use App\Services\TelegramServices;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;


class SendTelegMsgListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    protected $telegramServices;
    public function __construct(TelegramServices $telegramServices)
    {
        $this->telegramServices = $telegramServices;
    }
    /**
     * Handle the event.
     *
     * @param SendMsgEvent $sendMsgEvent
     * @return void
     */
    public function handle(SendMsgEvent $sendMsgEvent)
    {
        $this->telegramServices->setReceiverID($sendMsgEvent->msgBusSchema->userid);
        $this->telegramServices->setMsg($sendMsgEvent->msgBusSchema->msg);
        $this->telegramServices->sendMsg();

    }
}
