<?php

namespace App\Listeners;

use App\Audit;
use App\Events\RecvdMsgEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;


class AuditInMsgListener
{
    /**
     * Create the event listener.
     *
     * @param Audit $audit
     */
    protected $audit;
    public function __construct(Audit $audit)
    {
        $this->audit = $audit;
    }
    /**
     * Handle the event.
     *
     * @param RecvdMsgEvent $sendMsgEvent
     * @return void
     */
    public function handle(RecvdMsgEvent $sendMsgEvent)
    {
        $this->audit->create(
            array(
                'name' => $sendMsgEvent->msgBusSchema->from,
                'direction' => $sendMsgEvent->msgBusSchema->direction,
                'message' => $sendMsgEvent->msgBusSchema->msg,
                'chatid' => $sendMsgEvent->msgBusSchema->userid
            )
        );

    }
}
