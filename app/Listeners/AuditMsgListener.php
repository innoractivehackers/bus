<?php

namespace App\Listeners;

use App\Audit;
use App\Events\SendMsgEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;


class AuditMsgListener
{
    /**
     * Create the event listener.
     *
     * @param Audit $audit
     */
    protected $audit;
    public function __construct(Audit $audit)
    {
        $this->audit = $audit;
    }
    /**
     * Handle the event.
     *
     * @param SendMsgEvent $sendMsgEvent
     * @return void
     */
    public function handle(SendMsgEvent $sendMsgEvent)
    {
        $this->audit->create(
            array(
                'name' => $sendMsgEvent->msgBusSchema->from,
                'direction' => $sendMsgEvent->msgBusSchema->direction,
                'message' => $sendMsgEvent->msgBusSchema->msg,
                'chatid' => $sendMsgEvent->msgBusSchema->userid
            )
        );

    }
}
