<?php
namespace App\Services;

use App\Contracts\MsgBusContract;
use App\Schema\MsgBusSchema;
use Illuminate\Support\ServiceProvider;
use Telegram;

class TelegramServices extends ServiceProvider implements MsgBusContract
{
    public $receiver_id;
    protected $telegram;
    public $msg;
protected $msgBusSchema;
    public function __construct(MsgBusSchema $msgBusSchema) {
        $this->receiver_id = env('DEFAULT_USER_ID','');
        $this->msgBusSchema = $msgBusSchema;
    }

    public function init(){
        $this->telegram = new Telegram(env('BOT_TOKEN',''));
    }

    public function replyTestMsg(  $msg="" ){
        $this->init();
        $this->telegram->sendMessage(array(
                'chat_id' => $this->receiver_id,
                'text' => $msg
            ) );
    }
    public function setReceiverID($userid)
    {
        $this->receiver_id = $userid;
            }
    public function setMsg($msg)
    {
        $this->msg = $msg;
    }
    public function sendMsg()
    {
        $this->init();
        $this->telegram->sendMessage(array(
            'chat_id' => $this->receiver_id,
            'text' => $this->msg
        ) );
    }
}
