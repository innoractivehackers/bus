<?php
namespace App\Services;

use App\Contracts\MsgBusContract;
use App\Schema\MsgBusSchema;
use Illuminate\Support\ServiceProvider;


class LineServices extends ServiceProvider implements MsgBusContract
{
    public $receiver_id;
    protected $telegram;
    public $msg;
    protected $msgBusSchema;
    protected $channelSecret;
    protected $line;

    public function __construct(MsgBusSchema $msgBusSchema) {
        $this->channelSecret = getenv('LINEBOT_CHANNEL_SECRET') ?: 'ce3e35d12dfa02e9c140e6f4476f4b68';
        $this->msgBusSchema = $msgBusSchema;
    }

    public function init(){

        $channelToken = env('LINEBOT_CHANNEL_TOKEN','');

        $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient($channelToken);
        $this->line = new \LINE\LINEBot($httpClient, ['channelSecret' => $this->channelSecret]);
    }

    public function replyTestMsg(  $msg="" ){
        $this->init();
        $this->telegram->sendMessage(array(
            'chat_id' => $this->receiver_id,
            'text' => $msg
        ) );
    }
    public function setReceiverID($userid)
    {
        $this->receiver_id = $userid;
    }
    public function setMsg($msg)
    {
        $this->msg = $msg;
    }
    public function sendMsg()
    {
        $this->init();
        $textMessageBuilder = new \LINE\LINEBot\MessageBuilder\TextMessageBuilder($this->msg);
        $response = $this->line->pushMessage($this->receiver_id, $textMessageBuilder);
        if ($response->isSucceeded()) {
            return true;
        }
        else{
            return false;
        }
    }
}
