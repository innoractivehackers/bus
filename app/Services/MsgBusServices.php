<?php
namespace App\Services;

use App\Events\RecvdMsgEvent;
use App\Events\SendMsgEvent;
use App\Schema\MsgBusSchema;
use App\Subscriber;
use Exception;
use Illuminate\Support\ServiceProvider;
use App\Contracts\MsgBusContract;

class MsgBusServices extends ServiceProvider implements MsgBusContract
{
    protected $txt;
    protected $msgType;
    protected $subscriber;
    protected $emailParsingServices;
    protected $checkEntities;
    protected $msgBusSchema;
    protected $queue_job = [];

    const START = '/start';
    const HELP = '/help';
    const CHANNEL = '/channel';
    const SUBSCRIBE = '/sub';
    const LISTSUBSCRIBE = '/listsub';
    const UNSUBSCRIBE = '/unsub';

    public function __construct(Subscriber $subscriber,
                                EmailParsingServices $emailParsingServices,
                                MsgBusSchema $msgBusSchema)
    {
        $this->subscriber = $subscriber;
        $this->emailParsingServices = $emailParsingServices;
        $this->msgBusSchema = $msgBusSchema;
    }

    public function parseCommand( $r ){
        $this->parseIncomingMsg( $r );
        $this->msgBusSchema->direction = 'IN';
        $this->msgBusSchema->msg = $this->txt;
        event( new RecvdMsgEvent( $this->msgBusSchema ));
        if( $this->checkEntities != null ) {
            $tokens = mbsplit(" ",$this->txt);
            switch ($this->msgType) {
                case "bot_command":
                    $this->botCmd( $tokens );
                    break;
            }
        }
        else
        {
            $this->setMsg( 'Wrong command');
        }
        $this->setReceiverID( $this->msgBusSchema->userid );
        $this->sendMsg();
    }

    public function parseIncomingMsg( $r ){
        if( self::parseIncomingMsgParameter($r, 'message') != null) {
            $this->msgBusSchema->from = self::parseIncomingMsgParameter($r['message']['from'], 'first_name').' '.self::parseIncomingMsgParameter($r['message']['from'], 'last_name');
            $this->msgBusSchema->userid = self::parseIncomingMsgParameter($r['message']['from'], 'id');
            $this->txt = self::parseIncomingMsgParameter($r['message'], 'text');
            $this->msgType = self::parseIncomingMsgParameter($r['message']['entities'][0], 'type');
            $this->checkEntities = self::parseIncomingMsgParameter($r['message'], 'entities');
        }
    }

    public function botCmd( $cmd ){
        switch ( $cmd[0] ) {
            case self::START:
            case self::HELP:
                $this->setMsg( trans('help.menu') );
                break;
            case self::CHANNEL:
                break;
            case self::SUBSCRIBE:
                if(count($cmd)>1)
                    $this->subscribe($cmd);
                else
                    $this->listOfSubscribe();
                break;
            case self::LISTSUBSCRIBE:
                $this->listOfSubscribe();
                break;
            case self::UNSUBSCRIBE:
                $this->unsubscribe($cmd);
                break;
            default:
                $this->setMsg( trans('help.menu') );
                break;
        }
    }
    public function subscribe( $cmd ){
        $channel = $cmd[1];
        $this->subscriber->ch_name = $channel;
        $this->subscriber->uid = $this->msgBusSchema->userid;
        if ( $this->subscriber->checkIfExist() )
        {
            $this->setMsg( trans('help.channel').' `'.$channel.'` '.trans('help.exist') );
        }
        else {
            $status = $this->subscriber
                ->store(array(
                    'channel_name' => $channel,
                    'userid' => $this->msgBusSchema->userid,
                    'username' => $this->msgBusSchema->from
                ));
            if($status) {
                $this->setMsg(trans('help.channel').' `'.$channel.'` '.trans('help.create_success'));
            }
            else {
                $this->setMsg(trans('help.channel').' `'.$channel.'` '.trans('help.fail_create'));
            }
        }
    }
    public function unsubscribe( $cmd ){
        $channel = $cmd[1];
        $this->subscriber->ch_name = $channel;
        $this->subscriber->uid = $this->msgBusSchema->userid;
        if ( $this->subscriber->checkIfExist() )
        {
            $this->subscriber->unsubscribe();
            $this->setMsg(trans('help.channel').' `'.$channel.'` '.trans('help.unsubscribe'));
        }
        else
        {
            $this->setMsg(trans('help.channel').' `'.$channel.'` '.trans('help.not_exist'));
        }
    }

    public function listOfSubscribe(){
        $this->subscriber->uid = $this->msgBusSchema->userid;
        $combined_channel = '';
        $listsubscribe = $this->subscriber->userChannel()->get();
        foreach( $listsubscribe as $lsub ){
            $channel_in_array[]= $lsub->channel_name;
        }
        try {
            if(is_array($channel_in_array)) {
                $combined_channel = join(',', $channel_in_array);
            }
        }
        catch (Exception $ex){
            $combined_channel = '';
        }
        $this->setMsg(trans('help.channel_list').': '.$combined_channel);
    }

    static function parseIncomingMsgParameter (array $parameters, $parameter){
        try {
            $x = $parameters[$parameter];
        }
        catch (Exception $ex){
            $x = null;
        }
        return $x;
    }

    public function setMsg($msg){
        $this->msgBusSchema->msg = $msg;
        return $this;
    }
    /*
     * Set destination receiver ID
     */
    public function setReceiverID( $userid ){
        $this->msgBusSchema->userid = $userid;
    }
    /*
     * Generate SendMsg event
     */
    public function sendMsg(){
        $this->msgBusSchema->direction = 'OUT';
        event( new SendMsgEvent( $this->msgBusSchema ));
    }
    public function parseEmail( $r ){
        $to = $r['To'];
        $emails = $this->emailParsingServices->extract($to);
        foreach($emails as $email){
            $e = $this->emailParsingServices->onlyEmail($email);
            $this->queueMsg($e, $r, $to);
        }
        $this->processQueueMsg();
    }
    public function queueMsg( $channelInArray, $r, $to ){
        $from =$r['from'];
        $subject = $r['subject'];
        $msg = $r['body-plain'];

        $listsubscribers = $this->subscriber->generateHierachySQL($channelInArray)->get();
        foreach($listsubscribers as $listsubscriber)
        {
            $this->queue_job[$listsubscriber->userid]['from'] = $from;
            $this->queue_job[$listsubscriber->userid]['to'] = $to;
            $this->queue_job[$listsubscriber->userid]['subject'] = $subject;
            $this->queue_job[$listsubscriber->userid]['msg'] = $msg;
        }
    }
    public function processQueueMsg(){
        foreach($this->queue_job as $chatID=>$items) {
            $this->setReceiverID( $chatID );
            $this->setMsg('From: '.$items['from'].PHP_EOL
                .'To: '.$items['to'].PHP_EOL
                .'Subject: ['.$items['subject'].']'.PHP_EOL
                .$items['msg']);
            $this->sendMsg();
        }
    }
}

