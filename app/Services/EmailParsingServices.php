<?php
namespace App\Services;


use Illuminate\Support\ServiceProvider;
use function preg_match_all;
use function rtrim;

class EmailParsingServices extends ServiceProvider
{



    public function __construct() {

    }


    public function extract( $string ){
        preg_match_all("/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i", $string, $matches);
        return $matches[0];
    }
    public function onlyEmail( $string ){
        preg_match_all("/[\._a-zA-Z0-9-]+@+/i", $string,$matches);
        $matches[0][0] = rtrim( $matches[0][0],'@');
        return $matches[0][0];
    }
}
