<?php

namespace App\Http\Controllers;

use App\Services\MsgBusServices;
use Illuminate\Http\Request;

class BusController extends Controller
{
    protected $msgBusServices ;

    public function __construct(MsgBusServices $msgBusServices)
    {
        $this->msgBusServices = $msgBusServices;
    }

    public function testMsg(){
        $this->msgBusServices
            ->setMsg("Tested working")
            ->sendMsg();
        echo 'Success send test telegram msg';
    }

    public function webhook(Request $r){        
        $this->msgBusServices->parseCommand( $r->all() );
        echo 'OK';
    }

    public function testWebhook(){
        echo 'HTTP get webhook success.';
    }

    public function mailhook(Request $r){
        $this->msgBusServices->parseEmail( $r->all() );
        echo 'OK';
    }

    public function testMailhook(){
        echo 'HTTP get mailhook success';
    }
    
    //
}
