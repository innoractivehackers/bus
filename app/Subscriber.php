<?php

namespace App;

use function explode;
use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userid',
        'channel_name',
        'username'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public $ch_name;
    public $uid;

    protected $hidden = [
        'password',
    ];
    function commonCriteria(){
        return $this->where('channel_name','=',$this->ch_name)
            ->where('userid','=',$this->uid);
    }
    function userChannel(){
        return $this->where('userid','=',$this->uid);
    }
    function checkIfExist( ){
        return $this->commonCriteria()->count();
    }
    function unsubscribe(){
        return $this->commonCriteria()->delete();
    }
    /*
     * Store new channel name if not exist.
     * */
    function store( array $attributes = [] ){
        $status =$this->firstOrCreate($attributes);
        return $status;
    }
    /*
     * Generate SQL query by hierarchy.
     */
    function generateHierachySQL( $channel ){
        $tokens = explode(".",$channel);
        $str = '';
        $query = $this;
        foreach($tokens as $token) {
            $str .= $token;
            $query =  $query->orWhere('channel_name', '=',$str);
            $str .= '.';
        }
        return $query;
    }
}
