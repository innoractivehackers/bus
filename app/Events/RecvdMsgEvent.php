<?php

namespace App\Events;

use App\Schema\MsgBusSchema;

class RecvdMsgEvent extends Event
{
    /**
     * Create a new Message event instance.
     *
     * @return void
     */
    public $msgBusSchema;
    public function __construct( MsgBusSchema $msgBusSchema)
    {
        $this->msgBusSchema = $msgBusSchema;
    }
}
