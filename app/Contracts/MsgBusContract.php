<?php

namespace App\Contracts;

interface MsgBusContract
{
    public function setReceiverID( $userid );
    public function setMsg( $msg );
    public function sendMsg();
}
