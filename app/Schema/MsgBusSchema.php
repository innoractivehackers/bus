<?php

namespace App\Schema;

class MsgBusSchema
{
   /**
     * The userid for sending.
     *
     * @var string
     */
    public $userid;

    /**
     * The msg for sending.
     *
     * @var string
     */
    public $msg;

    /*
     * Sender ID
     */
    public $from;

    /*
     * For audit trail msg direction. Default is OUT.
     */
    public $direction;
}
